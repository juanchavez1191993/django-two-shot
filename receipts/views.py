from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptsForm, ExpenseCategoryForm, AccountForm
# Create your views here.


@login_required


def receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": receipts,
    }
    return render(request, 'receipts/list.html', context)


def create_receipt(request):
    if request.method == "POST":
        form = ReceiptsForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptsForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"catgory_list": category}
    return render(request, "receipts/expense.html", context)


def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {"account_list": account}
    return render(request, "receipts/account.html", context)


def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/expense_create.html", context)


def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/account_create.html", context)
